# Pong

Test the wiring connection and IR sensor with pong.

This repo contains a small shell script that can be "installed" on any 
Raspberry Pi. When enabled as boot as a cron job, the two LEDs can provide 
valuable information to test the new IR sensor and the wiring connection in 
general.

If this does not work, the IR sensor is having problems and should be debugged 
or there is wiring issues. This code works on the Raspberry Pi 3B+, at the time 
of writing.

# Changelog

* Initial Commit
