#!/bin/sh
# Ping Pi Tester Code
# Run as root user to access the LEDs
#
# This is a tester code that should run on boot. The initial boot sequence for 
# the OS will have a solid red power light. After that is complete, we will have
# a red blinking power light for 60 seconds. The green activity light should be
# off. Once the red power light is solid again, the device is ready to read
# from the sensor. 
#
# When the green status light is solid, it is reading a high (motion detected).
# When the green status light is off, it is reading a low (no motion detected).
#
# LED colors
# echo 0/1 low/high to /sys/class/gpio/ledX/brightness
# led1 Green
# led0 Red
#
# All pins, initially are not meant to be read from. To enable that,
# echo the pin value into /sys/class/gpio/export. To reverse that echo to
# unexport instead.
#
# Created by David Tran
#
# Installation:
# https://rahulmahale.wordpress.com/2014/09/03/solved-running-cron-job-at-reboot-on-raspberry-pi-in-debianwheezy-and-raspbian/
#
# Schematic:
# https://www.jameco.com/Jameco/workshop/circuitnotes/raspberry_pi_circuit_note_fig2.jpg
#
# Reference:
# https://www.raspberrypi.org/forums/viewtopic.php?t=27830
# https://www.raspberrypi.org/forums/viewtopic.php?t=12530

main () {

  local current_empty_count
  local current_activation_count
  local read_value

  # This sets how many "no motioned detected" until we change the status to
  # empty room
  readonly when_room_is_empty=600

  current_empty_count=0

  # Sets the correct pin to read from
  echo "4" > /sys/class/gpio/export 2> /dev/null

  # Initialization Loop
  echo 0 > /sys/class/leds/led0/brightness
  echo 0 > /sys/class/leds/led1/brightness

  # Initial Sleep
  for i in $(seq 1 59); do
    echo "$((i & 1))" > /sys/class/leds/led1/brightness
    #printf "Please wait for %02d second(s)\n" "$((60 - $i))"
    sleep 1
  done

  # Set LED to solid value when initializtation ready
  echo 1 > /sys/class/leds/led1/brightness

  while :; do

    read_value="$(cat '/sys/class/gpio/gpio4/value')"
    echo "$read_value" > /sys/class/leds/led0/brightness

    if [ "$read_value" = 1 ]; then

       current_empty_count=0
       current_activation_count="$((current_activation_count+1))"

       #printf "Motion Detected Count %07d\n" "$current_activation_count"
    else

       current_activation_count=0
       current_empty_count="$((current_empty_count+1))"

       if [ "$current_empty_count" -lt "$when_room_is_empty" ]; then
         #printf "No Motion Detected %07d\n" "$current_empty_count"
         :
       else
         #printf "Empty Room Count %07d\n" "$current_empty_count"
         :
       fi
   fi

  sleep 0.5
  done

}

main
